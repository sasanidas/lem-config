(in-package :lem)
;; vi-mode configuration
(lem-vi-mode:vi-mode)

;; Load main theme
(load-theme "chalk")

;; Set the default package to LEM
(lem-lisp-mode/internal::lisp-set-package "LEM")

;; Load paredit
(lem-paredit-mode:paredit-mode)

(add-hook lem:*find-file-hook*
          (lambda (buffer)
              (change-buffer-mode buffer 'lem-paredit-mode:paredit-mode t)))


(defparameter *fer/vi-exit-q-buffers* '("*peek-source*"))

(define-command fer/exit-vi-buffer ()()
  (let* ((buffer (current-buffer))
         (buffer-name (buffer-name buffer)))
    (when (find buffer-name *fer/vi-exit-q-buffers* 
                :test 'string-equal)
      (delete-active-window)))) 

;; My own functions
(define-command fer/maybe-up-directory () ()
  (if (string-equal (lem:mode-name (lem:buffer-major-mode (lem:current-buffer)))
                      "Directory")
    (lem/directory-mode::directory-mode-up-directory)
    (lem-vi-mode/commands:vi-previous-line)))

(define-command kill-buffer-and-window () ()
  (kill-buffer (current-buffer))
  (delete-window (current-window)))

(define-command open-init-file () ()
  (find-file 
   (merge-pathnames "init.lisp" (lem-home))))

#+sbcl
(ql:quickload :slynk)
#+sbcl
(define-command start-slynk () ()
  (slynk:create-server :dont-close t))

(define-command emacs-state () ()
  (if (string-equal "vi"
       (mode-name (lem-core::current-global-mode)))
      (lem-core::emacs-mode)
      (lem-vi-mode:vi-mode)))


;; scheme config 
(defvar *swank-port* 4015)

#+sbcl
(setf lem-scheme-mode::*default-port* *swank-port*
      lem-scheme-mode:*use-scheme-autodoc* :auto
      lem-scheme-mode:*scheme-swank-server-run-command* 
      `("guile" "--r7rs"
		"-l" ".lem/lisp/scheme/r7rs-swank/guile-swank.scm" 
		"-c" 
		,(format nil "(import (guile-swank)) (start-swank ~a)" 
			 *swank-port*))
      lem-scheme-mode:*use-scheme-repl-shortcut* t ) 


;; Elixir configuration
(defvar *fer/elixir-lsp-path*
  '("sh" "/home/fermin/Programming/elixir-lsp/language_server.sh"))
#+sbcl
(setf (lem-lsp-mode/spec:spec-command
       (lem-lsp-mode/spec:get-language-spec 'lem-elixir-mode:elixir-mode))
      *fer/elixir-lsp-path*)

;; Javascript configuration

#+sbcl
(lem-lsp-mode/lsp-mode::define-language-spec 
    (js-spec lem-js-mode:js-mode)
  :language-id "javascript"
  :root-uri-patterns '("package.json" "tsconfig.json")
  :command '("typescript-language-server" "--stdio")
  :install-command "npm install -g typescript-language-server typescript"
  :readme-url "https://github.com/typescript-language-server/typescript-language-server"
  :connection-mode :stdio)


;; Keybindings

(define-key lem-lisp-mode:*lisp-mode-keymap* "C-j" 'lem-lisp-mode/eval::lisp-eval-last-expression-and-insert)

(define-key lem-vi-mode:*command-keymap* "g d" 'lem/thingatp::open-at-point)

(define-key lem-vi-mode:*command-keymap* "C-t" 'lem/go-back:go-back-global)
(define-key lem-vi-mode:*command-keymap* "q" 'fer/exit-vi-buffer)
(define-key lem-vi-mode:*command-keymap* "-" 'fer/maybe-up-directory)

(define-key *global-keymap* "C-h f" 'fer/lisp-describe-symbol)
(define-key *global-keymap* "C-h v" 'fer/lisp-describe-symbol)

(define-key *global-keymap* "C-h k" 'describe-key)

(define-key *global-keymap* "C-x u" 'lem-vi-mode/binds::vi-undo)

(define-key *global-keymap* "C-x 4 0" 'kill-buffer-and-window)
(define-key *global-keymap* "C-x d" 'find-file)

(define-key *global-keymap* "C-x l" 'lem/filer::filer)
(define-key *global-keymap* "C-@" 'execute-command)
(define-key *global-keymap* "C-Space" 'execute-command)

(define-key *global-keymap* "C-c u" 'lem-shell-mode::run-shell)

;; Same as GNU Emacs evil-mode
(define-key *global-keymap* "C-z" 'emacs-state)



(defun show-description (string)
  (let ((buffer (make-buffer "*lisp-description*")))
    (change-buffer-mode buffer 'lem-lisp-mode::lisp-mode)
    (with-pop-up-typeout-window (stream buffer :erase t)
      (princ string stream))))

(defun lisp-eval-describe (form)
  (lem-lisp-mode/internal::lisp-eval-async form #'lem::show-description))

(define-command fer/lisp-describe-symbol () ()
  (lem-lisp-mode/internal::check-connection)
  (let ((symbol-name
          (lem-lisp-mode/internal::prompt-for-symbol-name "Describe symbol: "
							  (or (symbol-string-at-point (current-point)) ""))))
    (when (string= "" symbol-name)
      (editor-error "No symbol given"))
    (lisp-eval-describe `(micros:describe-symbol ,symbol-name))))
